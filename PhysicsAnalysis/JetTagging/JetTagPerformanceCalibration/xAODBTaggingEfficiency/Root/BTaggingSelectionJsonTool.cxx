/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "xAODBTaggingEfficiency/BTaggingToolUtil.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionJsonTool.h"
#include <fstream>

BTaggingSelectionJsonTool::BTaggingSelectionJsonTool( const std::string & name)
  : asg::AsgTool( name )
{
  m_initialised = false;
  declareProperty( "MaxEta", m_maxEta = 2.5 );
  declareProperty( "MinPt", m_minPt = -1 /*MeV*/);
  declareProperty( "TaggerName",                    m_taggerName="",       "tagging algorithm name");
  declareProperty( "JetAuthor",                     m_jetAuthor="",        "jet collection");
  declareProperty( "OperatingPoint",                m_OP="",               "operating point");
  declareProperty( "JsonConfigFile",                m_json_config_path="", "Path to JSON config file");
}

StatusCode BTaggingSelectionJsonTool::initialize() {
  m_initialised = true;
  
  std::ifstream jsonFile(m_json_config_path);
  if (!jsonFile.is_open()) {
    ATH_MSG_ERROR( "JSON file " + m_json_config_path + " do not exist. Please put the correct path of the file." );
    return StatusCode::FAILURE;
  }
  m_json_config = json::parse(jsonFile);
  jsonFile.close();

  if (m_taggerName=="" || !m_json_config.contains(m_taggerName)){
    ATH_MSG_ERROR( " Tagger " + m_taggerName + " not found in JSON file: " + m_json_config_path );
    return StatusCode::FAILURE;
  }

  if (m_jetAuthor=="" || !m_json_config[m_taggerName].contains(m_jetAuthor)){
    ATH_MSG_ERROR( "Tagger: " +m_taggerName+ " and Jet Collection: " +m_jetAuthor+ " not found in JSON file: " +m_json_config_path );
    return StatusCode::FAILURE;
  }

  if (m_OP=="" || !m_json_config[m_taggerName][m_jetAuthor].contains(m_OP)){
    ATH_MSG_ERROR( "OP " +m_OP+ " not available for " +m_taggerName+ " tagger.");
    return StatusCode::FAILURE;
  }

  m_target = m_json_config[m_taggerName][m_jetAuthor]["meta"]["TaggingTarget"];

  // pre-load fraction values
  m_fractionAccessors.clear();
  for (const auto& outclass : m_json_config[m_taggerName][m_jetAuthor]["meta"]["categories"]) {
    std::string outclassStr = std::string(outclass);
    float fraction = m_json_config[m_taggerName][m_jetAuthor]["meta"]["fraction_" + outclassStr].get<float>();
    SG::AuxElement::ConstAccessor<float> accessor(m_taggerName + "_p" + outclassStr);
    bool isTarget = (outclassStr == m_target);
    m_fractionAccessors.emplace_back(fraction, accessor, isTarget);
  } 

  // pre-load cut values
  m_OPCutValues.clear();
  m_pTbins.clear();
  m_massbins.clear();
  auto& pT_mass_2d_cutvalue = m_json_config[m_taggerName][m_jetAuthor][m_OP]["pT_mass_2d_cutvalue"];

  for (const auto& pt : pT_mass_2d_cutvalue["pTbins"]) {
    m_pTbins.push_back(BTaggingToolUtil::getExtendedFloat(pt));
  }

  for (const auto& item : pT_mass_2d_cutvalue.items()) {
    std::string pT_key = item.key();
    if ( pT_key == "pTbins" ) continue;
    std::vector<float> mass_values = pT_mass_2d_cutvalue[pT_key]["mass"].get<std::vector<float>>();
    std::vector<float> cut_values = pT_mass_2d_cutvalue[pT_key]["cutvalues"].get<std::vector<float>>();
    m_massbins.push_back(mass_values);
    m_OPCutValues.push_back(cut_values);
  }   

  return StatusCode::SUCCESS;
}

double BTaggingSelectionJsonTool::getTaggerDiscriminant ( const xAOD::Jet& jet) const{

  float numerator = 0.;
  float denominator = 0.;
  for ( const auto& frac : m_fractionAccessors ) {
    float p_output = frac.accessor( jet ); 
    if ( frac.isTarget ) {
      numerator += frac.fraction * p_output;
    } else {
      denominator += frac.fraction * p_output;
    }
  }

  double tagger_discriminant = log(numerator / denominator); 

  return tagger_discriminant;
}

int BTaggingSelectionJsonTool::accept( const xAOD::Jet& jet ) const {
  ///////////////////////////////////////////////
  // Cheatsheet:
  // For fix cut WP, return 0 for not tagged, 1 for tagged
  ////////////////////////////////////////////////
  return accept( jet.pt(), jet.eta(), jet.m(), getTaggerDiscriminant(jet) );
}

int BTaggingSelectionJsonTool::accept( double pt, double eta, double mass, double tagger_discriminant ) const {

  if ( !m_initialised ) {
    throw std::runtime_error("BTaggingSelectionJsonTool has not been initialised.");
  }

  int index = 0;

  if ( std::abs(eta) > m_maxEta || pt < m_minPt ) {
    return index;
  }

  int pt_bin_index = findBin(m_pTbins, pt/1000.);
  if (pt_bin_index == -1) {
    return index;
  }

  int mass_bin_index = findBin(m_massbins[pt_bin_index], mass/1000.);
  if (mass_bin_index == -1) {
    return index;
  }

  float cutvalue = m_OPCutValues[pt_bin_index][mass_bin_index];
  index = (tagger_discriminant > cutvalue) ? 1 : 0;
  return index;

}

int BTaggingSelectionJsonTool::findBin(const std::vector<float>& bins, float value) const {
  for (size_t i = 0; i < bins.size()-1; i++) {
    if ((std::min(bins[i], bins[i+1]) <= value && value < std::max(bins[i], bins[i+1]))) {
      return i;
    }
  }
  return -1;
}
