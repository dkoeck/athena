# AthExMonitored
Package showing the use of the common online/offline [Monitored](https://atlassoftwaredocs.web.cern.ch/guides/trigger/monitoring/) framework.

To run the example algorithm and browse the histograms:

```sh
python -m AthExMonitored.MonitoredConfig
rootbrowse expert-monitoring.root
```
